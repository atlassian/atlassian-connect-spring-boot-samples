# Atlassian Connect Spring Boot Samples

This repository contains sample [Spring Boot](http://projects.spring.io/spring-boot/)
applications using [`atlassian-connect-spring-boot`](https://bitbucket.org/atlassian/atlassian-connect-spring-boot), the starter for building
[Atlassian Connect](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/) add-ons.

### Samples are now in `samples` branch

Samples are not in master branch anymore. You can find the samples and related links on the `samples` branch.
Please do `git checkout samples` to find them.

## Disclaimer

Most samples here use outdated dependency versions, which are likely to have known security vulnerabilities. Treat these samples as sources of inspiration; they are not ready for production.
Please upgrade to the latest versions before reporting issues with samples (and please consider submitting a pull request for the upgrade).

## License

This project is licensed under the [Apache License, Version 2.0](LICENSE.txt).
